package com.example.democrazy

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.ColorFilter
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.LayoutInflater
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable
import java.net.Socket

/* extend our MainActivity to implement our TCPClientListener interface */
class MainActivity : AppCompatActivity(), TCPClient.TCPClientListener {

    /* Global variables */
    private var mTCPClient = TCPClient.getInstance(this)
    private var isBackKeyPressTwice = false
    private var connectionStatus = NetworkConnectionStatus.DISCONNECTED
    private var motionStatus = MotionStatus.STOPPED
    private var prev_command : Int = 0
    private var notificationId = 0
    private var send_speed_dec = false
    private var send_speed_inc = false


    /* Override onCreate Method to setup our own UI */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /* Call our UI here */
        setContentView(R.layout.activity_main)

        if (!isTaskRoot()) {
            /* This is a known Android bug! Where in Android
             * launched another instance of the root activity into an existing task
             * so just quietly finish and go away, dropping the user back into the activity
             * at the top of the stack (ie: the last state of this tsask)
             */
            finish()
            return
        }

        /* This will create a notification channel, so we can push our notifications to the user */
        createNotificationChannel()

	startButton.setOnClickListener {
	    if (motionStatus == MotionStatus.STOPPED)
	    {
		Toast.makeText(this, "Starting motor. Please wait.", Toast.LENGTH_SHORT).show()
                Thread (Runnable {mTCPClient.sendMessage("START\n")}).start()
		motionStatus = MotionStatus.RUNNING
		startButton.setText("Stop")
	    }
	    else
	    {
		Toast.makeText(this, "Stopping motor. Please wait.", Toast.LENGTH_SHORT).show()
                Thread (Runnable {mTCPClient.sendMessage("STOP\n")}).start()
		motionStatus = MotionStatus.STOPPED
		startButton.setText("Start")
	    }
	}

        /* Connect Button event listener. */
	connectButton.setOnClickListener {
            if (connectionStatus == NetworkConnectionStatus.DISCONNECTED)
	    {
	        val mDialogView = LayoutInflater.from(this).inflate(R.layout.connect_dialog, null)
	        val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setTitle("Connect to Socket")
	        val mAlertDialog = mBuilder.show()
	        val doneButton: Button = mDialogView.findViewById(R.id.doneButton) as Button
	        val cancelButton: Button = mDialogView.findViewById(R.id.cancelButton) as Button
		val addressText: EditText = mDialogView.findViewById(R.id.socketAddress) as EditText
		val portText: EditText = mDialogView.findViewById(R.id.socketPort) as EditText

	        doneButton.setOnClickListener {
                    Toast.makeText(this, "Getting you connected. Please wait.", Toast.LENGTH_SHORT).show()

		    val socketAddress = addressText.getText().toString()
		    val socketPort = portText.getText().toString().toIntOrNull()

		    if (socketPort !== null) {
	                mTCPClient.setSocket(socketAddress, socketPort)
                        Thread(mTCPClient).start()
		        mAlertDialog.dismiss()
                        connectionStatus = NetworkConnectionStatus.CONNECTING
                        connectButton.setText("Cancel")
		    } else {
		        Toast.makeText(this, "Invalid socket port", Toast.LENGTH_SHORT).show()
		    }
	        }

	        cancelButton.setOnClickListener {
	            mAlertDialog.dismiss()
	        }
            }
            else if (connectionStatus == NetworkConnectionStatus.CONNECTED) {
                connectButton.setText("Connect")
                mTCPClient.getClient()?.close()
                connectionStatus = NetworkConnectionStatus.DISCONNECTED
                Toast.makeText(this,"Disconnected", Toast.LENGTH_SHORT).show()
            }
	    else
	    {
                mTCPClient.getClient()?.close()
                connectionStatus = NetworkConnectionStatus.DISCONNECTED
                wifiView.setColorFilter(Color.argb(255, 255, 0, 0))
                wifiText.setText("Disconnected")
                connectButton.setText("Connect")
                Toast.makeText(this@MainActivity, "User Canceled", Toast.LENGTH_SHORT).show()
	    }
	}

	lJoyStick.setOnMoveListener { angle, strength, event ->
	   //TODO

	   Log.d("LEFT", "Angle = " + angle.toString() + ", Strength = " + strength.toString())

	   if ((angle >= 0 && angle < 45) || (angle <= 359 && angle > 315))
	   {
               Thread (Runnable {mTCPClient.sendMessage("ARM_RIGHT\n")}).start()
	   }
	   else if(angle >= 45 && angle < 135)
	   {
               Thread (Runnable {mTCPClient.sendMessage("ARM_UP\n")}).start()
	   }
	   else if(angle >= 135 && angle < 225)
	   {
               Thread (Runnable {mTCPClient.sendMessage("ARM_LEFT\n")}).start()
	   }
	   else
	   {
               Thread (Runnable {mTCPClient.sendMessage("ARM_DOWN\n")}).start()
	   }
	}

        /* Joystick event listener */
        mJoyStick.setOnMoveListener { angle, strength, event ->
            if (connectionStatus == NetworkConnectionStatus.CONNECTED && auto_man_switch.text != "AUTO") {
                if (strength > 80) {
                    if (send_speed_inc == false) {
                        Thread (Runnable {mTCPClient.sendMessage("+\n")}).start()
                        send_speed_dec = false
                        send_speed_inc = true
                        prev_command = 9 /* restart the command to something */
                    }

                    if (angle in 45..135 && prev_command != 1 ) {
                        Thread (Runnable {mTCPClient.sendMessage("1\n")}).start()
                        prev_command = 1
                    }
                    else if (angle in 136..225 && prev_command != 2) {
                        Thread (Runnable {mTCPClient.sendMessage("2\n")}).start()
                        prev_command = 2
                    }
                    else if (angle in 226..315 && prev_command != 3) {
                        Thread (Runnable {mTCPClient.sendMessage("3\n")}).start()
                        prev_command = 3
                    }
                    else if ((angle < 45 || angle > 315) && prev_command != 4) {
                        Thread (Runnable {mTCPClient.sendMessage("4\n")}).start()
                        prev_command = 4
                    }
                }

                if (strength > 50 && strength < 80) {
                    if (send_speed_dec == false) {
                        Thread (Runnable {mTCPClient.sendMessage("-\n")}).start()
                        send_speed_inc = false
                        send_speed_dec = true
                        prev_command = 9 /* restart the command to something */
                    }

                    if (angle in 45..135 && prev_command != 1) {
                        Thread (Runnable {mTCPClient.sendMessage("1\n")}).start()
                        prev_command = 1
                    }
                    else if (angle in 136..225 && prev_command != 2) {
                        Thread (Runnable {mTCPClient.sendMessage("2\n")}).start()
                        prev_command = 2
                    }
                    else if (angle in 226..315 && prev_command != 3) {
                        Thread (Runnable {mTCPClient.sendMessage("3\n")}).start()
                        prev_command = 3
                    }
                    else if ((angle < 45 || angle > 315) && prev_command != 4) {
                        Thread (Runnable {mTCPClient.sendMessage("4\n")}).start()
                        prev_command = 4
                    }
                }

                else if (strength < 50 && prev_command != 0){
                    Thread (Runnable {mTCPClient.sendMessage("0\n")}).start()
                    prev_command = 0
                }

                if (event != null && event.action == MotionEvent.ACTION_UP) {
                    Thread (Runnable {mTCPClient.sendMessage("0\n")}).start()
                    prev_command = 0
                }
            }
        }

        thermal_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                Thread (Runnable {mTCPClient.sendMessage("5\n")}).start() /* Turn on Thermal */
            }
            else {
                Thread (Runnable {mTCPClient.sendMessage("6\n")}).start() /* Turn off Thermal */
            }
        }

        auto_man_switch.setOnClickListener {
            /* For Demo purposes only!
             * if (auto_man_switch.text == "SET MODE") {
             *  auto_man_switch.setText("MAN")
             *  videoView.loadUrl("http://192.168.8.1:8000")
             *  videoView.visibility = View.VISIBLE
             *  videoView.loadUrl("http://192.168.8.1:8000")
             *  temperatureTextView.visibility = View.VISIBLE
             *  distanceTextView.visibility = View.VISIBLE
             * }
             */

            if (auto_man_switch.text == "MAN") {
                auto_man_switch.setText("AUTO")
                Thread (Runnable {mTCPClient.sendMessage("7\n")}).start() /* Turn on AUTO mode */
            }

            else if (auto_man_switch.text == "AUTO") {
                auto_man_switch.setText("MAN")
                Thread (Runnable {mTCPClient.sendMessage("8\n")}).start() /* Turn off AUTO mode */
            }
        }

        /**
        mExitButton.setOnClickListener {
            if (mTCPClient.getClient() != null) {
                mTCPClient.getClient()?.close()
                Log.d("debug", "socket closed")
            }
            finishAffinity()
        }
	**/

    } /* End of onCreate */


    /*
     * Override the TCPClientListener interface here and handle incoming messages from the server.
     * We need to run this on the Main thread(UI/Main Activity) so that changes will
     * reflect to the main UI thread since this interface is run on a worker thread.
     *
     * Fix Me.
     * Sometimes this is not run on the Main 'Active' Thread. Causing the app to look like
     * it is stack on waiting for the connection to happen although the connection
     * has been made already.
     *
     * Possible fix is to create a handler or call to the main looper.
     */
    override fun onMessageReceiveFromServer(msg: String) {
        Log.d("msg", msg)

        if (msg == "connected") {
            this.runOnUiThread {
                connectionStatus = NetworkConnectionStatus.CONNECTED
                thermal_switch.isChecked = false
                wifiView.setColorFilter(Color.argb(255, 0, 255, 0))
                wifiText.setText("100%")
                /* Initialize battery status to unknown first */
                batteryText.setText("UNKNOWN")
                connectButton.setText("Disconnect")
                Toast.makeText(this@MainActivity, "You are now connected", Toast.LENGTH_SHORT).show()
            }
        }

        else if (msg == "can't connect") {
            this.runOnUiThread {
                connectionStatus = NetworkConnectionStatus.DISCONNECTED
                Toast.makeText(this, "Can't connect to server", Toast.LENGTH_SHORT).show()
                mTCPClient.getClient()?.close()

                batteryText.setText("---%")
                wifiView.setColorFilter(Color.argb(255, 255, 0, 0))
                wifiText.setText("Disconnected")
                connectButton.setText("Connect")
            }
        }

        else {
            /* Server data will be divided into 'b' for battery and 'n' if it's just a status/notification report */
            val separate = msg.split ("-")
            Log.d("democrazy", separate[1])
            if (separate[0] == "b") {
                this.runOnUiThread {
                    updateBatteryStatus(separate[1])
                }
            }

            else if(separate[0] == "v") {
                this.runOnUiThread {
                    if (separate[1] == "thermal_on") {
                        thermal_switch.isChecked = true
                    }
                    else if (separate[1] == "thermal_off") {
                         thermal_switch.isChecked = false
                    }
                }
            }

            else if(separate[0] == "t") {
                this.runOnUiThread {
                    val temp = separate[1] + " C"
                    Log.d("dist", temp)
                }
            }

            else if(separate[0] == "d") {
                this.runOnUiThread {
                    val dist = separate[1] + " cm"
                    Log.d("dist", dist)
                }
            }

            else if (separate[0] == "n") {
                this.runOnUiThread {
                    /*
                     * We've received a status report from the robot, so populate this to the notification panel.
                     * This intent is for launching the app if the user tap the notification.
                     * No need to put some extra info, will just resume the state of the app.
                     * This is the same as pressing the Home button and launching the app again
                     * using the app icon or recent apps.
                     */
                    val pm : PackageManager = packageManager
                    var intent = pm.getLaunchIntentForPackage("com.example.democrazy")
                    val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
                    sendNotification(separate[1], pendingIntent)
                }
            }
        }
    }

    /* override the back button to not exit at first press */
    override fun onBackPressed() {
        if (isBackKeyPressTwice) {
            if (mTCPClient.getClient() != null) {
                mTCPClient.getClient()?.close()
                Log.d("debug", "socket closed")
            }
            finishAffinity()
            return
        }
        isBackKeyPressTwice = true
        Toast.makeText(this, "Press back again to confirm exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { isBackKeyPressTwice = false }, 2000)
    }


    /*
     * Function/Method definitions below
     */

    private fun updateBatteryStatus (value : String) {
        val batteryPercent = value + "%"
        batteryText.setText(batteryPercent)
        when (value.toInt()) {
            in 0..40 -> {
                batteryView.setColorFilter(Color.argb(255, 255, 0, 0))
                val pm : PackageManager = packageManager
                var intent = pm.getLaunchIntentForPackage("com.example.democrazy")
                val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
                sendNotification("Batter Low. Recharge the robot!", pendingIntent)
            }
            in 41..70 -> batteryView.setColorFilter(Color.argb(255, 255, 255, 0))
            else -> batteryView.setColorFilter(Color.argb(255, 0, 255, 0))
        }
    }

    private fun createNotificationChannel() {
        /* This is required for Android 8.0+ (Oreo and up) only.*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Notify DemoCrazy"
            val channel_id = "notify.democrazy"
            val descriptionText = "notifications from robot"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channel_id, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun sendNotification(msg: String, pendingIntent : PendingIntent) {
        val builder = NotificationCompat.Builder(this, "notify.democrazy")
            .setSmallIcon(R.drawable.rounded_buttons_profile)
            .setContentTitle("Robot status report")
            .setContentText(msg)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            // Set the intent that will fire when the user taps the notification
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setGroup("democrazy.group.notification")
            .setGroupSummary(true)

        with(NotificationManagerCompat.from(this)) {
            notify (notificationId++, builder.build())
        }
    }
} /* end of MainActivity */
