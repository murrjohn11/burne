package com.example.democrazy

enum class MotionStatus
{
    RUNNING,
    STOPPED
}
