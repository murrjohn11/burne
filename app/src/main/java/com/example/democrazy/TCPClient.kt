package com.example.democrazy

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.widget.Toast
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable
import java.lang.Exception
import java.net.InetAddress
import java.net.Socket

/*
 * Make this class a singleton. So that when resuming the app or creating new instances,
 * only the socket that we first initialize will be returned.
 * */
class TCPClient private constructor(context: Context) : Runnable {


    private lateinit var client : Socket
    lateinit var input : BufferedReader
    private var mContext = context
    private lateinit var mActivity : Activity
    private var mTCPClientListener: TCPClientListener
    private var toastShown = false
    private var address = "192.168.8.1"
    private var port = 1234

    companion object : SingletonHolder <TCPClient, Context>(::TCPClient)


    init {
        mTCPClientListener = mContext as TCPClientListener
        mActivity = mContext as Activity
    }

    fun setSocket(socketAddress:String, socketPort:Int)
    {
        address = socketAddress
	port = socketPort
    }

    /* A runnable to handle new connection socket and incoming messages. This will be run on a separate thread. */
    override fun run() {
        try {
            client = Socket(address, port)

            input = BufferedReader(InputStreamReader(client.getInputStream()))

            toastShown = true

            Log.d("democrazy", "connection establish")
            if (client.isConnected) {
                mTCPClientListener.onMessageReceiveFromServer("connected")
            }

            /* Create a loop here that will receive incoming messages from the server if we're still connected */
            while (!client.isClosed) {
                try {
                    /* Write the data to our interface so that we can parse it on our MainActivity */
                    mTCPClientListener.onMessageReceiveFromServer(input.readLine())
                } catch (e: Exception) {
                    /*
                     * This means that we might have recieve a null data and tried to write to a non-nullable
                     * argument of onMessageReceiveFromServer. Or the connection has been close. Do nothing for now.
                     */
                }
            }

            /* The loop has ended, this means the connection has been close. Notify the MainActivity to reset the network connection status*/
            mTCPClientListener.onMessageReceiveFromServer("can't connect")
            Log.d("democrazy", "finish receiving")

        } catch (e: Exception) {
            /* Notify the Main Activity we can't connect to the server */
            mTCPClientListener.onMessageReceiveFromServer("can't connect")
            Log.d("democrazy", "can't connect to server")
        }
    }

    /* Function to send message to the server */
    fun sendMessage(msg:String) {
        try {
            client.outputStream.write(msg.toByteArray(Charsets.UTF_8))
            toastShown = false
            Log.d ("democrazy", msg)
        }
        catch (e:Exception) {
            Log.e ("democrazy", "exception in sendMessage", e)
            toastShown = true
            mActivity.runOnUiThread {
                if (!toastShown) {
                    Toast.makeText(mContext, "Can't send command", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    /* Getter method for our socket to enable other class to close the connection */
    fun getClient() : Socket? {
        if (::client.isInitialized) {
            return client
        }
        else {
            return null
        }
    }

    /* A public interface for sending messages back to the Main thread */
    interface TCPClientListener {
        fun onMessageReceiveFromServer (msg: String)
    }

}
