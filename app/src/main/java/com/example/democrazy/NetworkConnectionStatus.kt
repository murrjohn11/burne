package com.example.democrazy

enum class NetworkConnectionStatus {
    CONNECTED,
    DISCONNECTED,
    CONNECTING
}