package com.bhargavms.dotloader

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator

import java.lang.ref.WeakReference

/**
 * Isn't this a cool animation?
 */
class DotLoader : View {
    private var mDots: Array<Dot>? = null
    internal var mColors: Array<Int>
    private var mDotRadius: Int = 0
    private var mClipBounds: Rect? = null
    private var mCalculatedGapBetweenDotCenters: Float = 0.toFloat()
    private var mFromY: Float = 0.toFloat()
    private var mToY: Float = 0.toFloat()
    private val bounceAnimationInterpolator = CubicBezierInterpolator(0.62f, 0.28f, 0.23f, 0.99f)

    fun setNumberOfDots(numberOfDots: Int) {
        val newDots = arrayOfNulls<Dot>(numberOfDots)
        if (numberOfDots < mDots!!.size) {
            System.arraycopy(mDots!!, 0, newDots, 0, numberOfDots)
        } else {
            System.arraycopy(mDots!!, 0, newDots, 0, mDots!!.size)
            for (i in mDots!!.size until numberOfDots) {
                newDots[i] = Dot(this, mDotRadius, i)
                newDots[i].cx = newDots[i - 1].cx + mCalculatedGapBetweenDotCenters
                newDots[i].cy = newDots[i - 1].cy / 2
                newDots[i].setColorIndex(newDots[i - 1].mCurrentColorIndex)
                newDots[i].positionAnimator =
                    clonePositionAnimatorForDot(newDots[0].positionAnimator!!, newDots[i])
                newDots[i].colorAnimator =
                    cloneColorAnimatorForDot(newDots[0].colorAnimator!!, newDots[i])
                newDots[i].positionAnimator!!.start()
            }
        }
        mDots = newDots
    }

    private fun cloneColorAnimatorForDot(colorAnimator: ValueAnimator, dot: Dot): ValueAnimator {
        val valueAnimator = colorAnimator.clone()
        valueAnimator.removeAllUpdateListeners()
        valueAnimator.addUpdateListener(DotColorUpdater(dot, this))
        return valueAnimator
    }

    private fun clonePositionAnimatorForDot(animator: ValueAnimator, dot: Dot): ValueAnimator {
        val valueAnimator = animator.clone()
        valueAnimator.removeAllUpdateListeners()
        valueAnimator.addUpdateListener(DotYUpdater(dot, this))
        valueAnimator.startDelay = (DELAY_BETWEEN_DOTS * dot.position).toLong()
        valueAnimator.removeAllListeners()
        valueAnimator.addListener(AnimationRepeater(dot, mColors))
        return valueAnimator
    }

    fun resetColors() {
        for (dot in mDots!!) {
            dot.setColorIndex(0)
        }
    }

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    @TargetApi(21)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(context, attrs)
    }

    private fun init(c: Context, attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val a = c.theme.obtainStyledAttributes(attrs, R.styleable.DotLoader, 0, 0)
        try {
            val dotRadius = a.getDimension(R.styleable.DotLoader_dot_radius, 0f)
            val numberOfPods = a.getInteger(R.styleable.DotLoader_number_of_dots, 1)
            val resId = a.getResourceId(R.styleable.DotLoader_color_array, 0)
            val colors: Array<Int>
            if (resId == 0) {
                colors = arrayOfNulls(numberOfPods)
                for (i in 0 until numberOfPods) {
                    colors[i] = 0x0
                }
            } else {
                val temp = resources.getIntArray(resId)
                colors = arrayOfNulls(temp.size)
                for (i in temp.indices) {
                    colors[i] = temp[i]
                }
            }
            init(numberOfPods, colors, dotRadius.toInt())
        } finally {
            a.recycle()
        }
    }

    private fun _stopAnimations() {
        for (dot in mDots!!) {
            dot.positionAnimator!!.end()
            dot.colorAnimator!!.end()
        }
    }

    private fun init(numberOfDots: Int, colors: Array<Int>, dotRadius: Int) {
        mColors = colors
        mClipBounds = Rect(0, 0, 0, 0)
        mDots = arrayOfNulls(numberOfDots)
        mDotRadius = dotRadius
        for (i in 0 until numberOfDots) {
            mDots[i] = Dot(this, dotRadius, i)
        }

        startAnimation()
    }

    fun initAnimation() {
        var i = 0
        val size = mDots!!.size
        while (i < size) {
            mDots!![i].positionAnimator = createValueAnimatorForDot(mDots!![i])
            mDots!![i].positionAnimator!!.startDelay = (DELAY_BETWEEN_DOTS * i).toLong()

            mDots!![i].colorAnimator = createColorAnimatorForDot(mDots!![i])
            i++
        }
    }

    /**
     * Won't support starting and stopping animations for now, until I figure out how to sync animation
     * delays.
     *
     */
    @Deprecated("")
    private fun startAnimation() {
        postDelayed({ _startAnimation() }, 10)
    }

    /**
     * Won't support starting and stopping animations for now, until I figure out how to sync animation
     * delays.
     *
     */
    @Deprecated("")
    private fun stopAnimations() {
        post { _stopAnimations() }
    }

    private fun _startAnimation() {
        initAnimation()
        for (mDot in mDots!!) {
            mDot.positionAnimator!!.start()
        }
    }

    private fun createValueAnimatorForDot(dot: Dot): ValueAnimator {
        val animator = ValueAnimator.ofFloat(
            mFromY, mToY
        )
        animator.interpolator = bounceAnimationInterpolator
        animator.duration = BOUNCE_ANIMATION_DURATION.toLong()
        animator.repeatCount = ValueAnimator.INFINITE
        animator.repeatMode = ValueAnimator.REVERSE
        animator.addUpdateListener(DotYUpdater(dot, this))
        animator.addListener(AnimationRepeater(dot, mColors))
        return animator
    }

    private fun createColorAnimatorForDot(dot: Dot): ValueAnimator {
        val animator = ValueAnimator.ofObject(
            ArgbEvaluator(), mColors[dot.mCurrentColorIndex],
            mColors[dot.incrementColorIndex()]
        )
        animator.interpolator = LinearInterpolator()
        animator.duration = COLOR_ANIMATION_DURATION.toLong()
        animator.addUpdateListener(DotColorUpdater(dot, this))
        return animator
    }

    private class DotColorUpdater private constructor(private val mDot: Dot, dotLoader: DotLoader) :
        ValueAnimator.AnimatorUpdateListener {
        private val mDotLoaderRef: WeakReference<DotLoader>

        init {
            mDotLoaderRef = WeakReference(dotLoader)
        }

        override fun onAnimationUpdate(valueAnimator: ValueAnimator) {
            mDot.setColor(valueAnimator.animatedValue as Int)
            val dotLoader = mDotLoaderRef.get()
            dotLoader?.invalidateOnlyRectIfPossible()
        }
    }

    private class DotYUpdater private constructor(private val mDot: Dot, dotLoader: DotLoader) :
        ValueAnimator.AnimatorUpdateListener {
        private val mDotLoaderRef: WeakReference<DotLoader>

        init {
            mDotLoaderRef = WeakReference(dotLoader)
        }

        override fun onAnimationUpdate(valueAnimator: ValueAnimator) {
            mDot.cy = valueAnimator.animatedValue as Float
            val dotLoader = mDotLoaderRef.get()
            dotLoader?.invalidateOnlyRectIfPossible()
        }
    }


    private fun invalidateOnlyRectIfPossible() {
        if (mClipBounds != null && mClipBounds!!.left != 0 &&
            mClipBounds!!.top != 0 && mClipBounds!!.right != 0 && mClipBounds!!.bottom != 0
        )
            invalidate(mClipBounds)
        else
            invalidate()
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.getClipBounds(mClipBounds)
        for (mDot in mDots!!) {
            mDot.draw(canvas)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val width: Int
        val height: Int
        // desired height is 6 times the diameter of a dot.
        val desiredHeight = mDotRadius * 2 * 3 + paddingTop + paddingBottom

        //Measure Height
        if (heightMode == View.MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize
        } else if (heightMode == View.MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize)
        } else {
            //Be whatever you want
            height = desiredHeight
        }
        mCalculatedGapBetweenDotCenters = calculateGapBetweenDotCenters()
        val desiredWidth = (mCalculatedGapBetweenDotCenters * mDots!!.size).toInt()
        //Measure Width
        if (widthMode == View.MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize
        } else if (widthMode == View.MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize)
        } else {
            //Be whatever you want
            width = desiredWidth
        }
        var i = 0
        val size = mDots!!.size
        while (i < size) {
            mDots!![i].cx = mDotRadius + i * mCalculatedGapBetweenDotCenters
            mDots!![i].cy = (height - mDotRadius).toFloat()
            i++
        }
        mFromY = (height - mDotRadius).toFloat()
        mToY = mDotRadius.toFloat()
        setMeasuredDimension(width, height)
    }

    private fun calculateGapBetweenDotCenters(): Float {
        return (mDotRadius * 2 + mDotRadius).toFloat()
    }

    companion object {
        private val DELAY_BETWEEN_DOTS = 80
        private val BOUNCE_ANIMATION_DURATION = 500
        private val COLOR_ANIMATION_DURATION = 80
    }
}
