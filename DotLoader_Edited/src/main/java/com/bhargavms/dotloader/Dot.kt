package com.bhargavms.dotloader

import android.animation.ValueAnimator
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint

/**
 * Created by Bhargav on 7/20/2016.
 */
internal class Dot(parent: DotLoader, private val mDotRadius: Int, var position: Int) {
    private val mPaint: Paint
    var mCurrentColorIndex: Int = 0
    private val mColors: Array<Int>
    var cx: Float = 0.toFloat()
    var cy: Float = 0.toFloat()
    var positionAnimator: ValueAnimator? = null
    var colorAnimator: ValueAnimator? = null

    private val currentColor: Int
        get() = mColors[mCurrentColorIndex]

    init {
        mColors = parent.mColors
        mCurrentColorIndex = 0

        mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        mPaint.color = mColors[mCurrentColorIndex]
        //mPaint.setShadowLayer(5.5f, 6.0f, 6.0f, Color.BLACK);
        mPaint.style = Paint.Style.FILL
    }

    fun setColorIndex(index: Int) {
        mCurrentColorIndex = index
        mPaint.color = mColors[index]
    }

    fun setColor(color: Int) {
        mPaint.color = color
    }

    fun incrementAndGetColor(): Int {
        incrementColorIndex()
        return currentColor
    }

    fun applyNextColor() {
        mCurrentColorIndex++
        if (mCurrentColorIndex >= mColors.size)
            mCurrentColorIndex = 0
        mPaint.color = mColors[mCurrentColorIndex]
    }

    fun incrementColorIndex(): Int {
        mCurrentColorIndex++
        if (mCurrentColorIndex >= mColors.size)
            mCurrentColorIndex = 0
        return mCurrentColorIndex
    }

    fun draw(canvas: Canvas) {
        canvas.drawCircle(cx, cy, mDotRadius.toFloat(), mPaint)
    }
}
