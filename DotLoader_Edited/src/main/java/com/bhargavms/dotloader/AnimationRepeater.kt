package com.bhargavms.dotloader

import android.animation.Animator

/**
 * Created by bhargav on 5/27/17.
 */
internal class AnimationRepeater(private val mDot: Dot, private val mColors: Array<Int>) :
    Animator.AnimatorListener {
    private var alternate = true

    override fun onAnimationStart(animator: Animator) {

    }

    override fun onAnimationEnd(animator: Animator) {

    }

    override fun onAnimationCancel(animator: Animator) {

    }

    override fun onAnimationRepeat(animator: Animator) {
        if (alternate) {
            mDot.colorAnimator!!.setObjectValues(
                mColors[mDot.mCurrentColorIndex],
                mColors[mDot.incrementColorIndex()]
            )
            mDot.colorAnimator!!.start()
            alternate = false
        } else {
            alternate = true
        }
    }
}
